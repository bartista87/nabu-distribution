<?php
if (!defined('TYPO3')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dtinabu_domain_model_slide');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('dti_nabu', 'Configuration/TypoScript', 'NABU Template 2016');

// Add Slideshow
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'dti_nabu',
    'DtiNabuSlideshow',
    'NABU Startseiten Slideshow'
);

### Register SVG Icons for BackendContentElements
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

// Icon for Blue Box
$iconRegistry->registerIcon('dti_nabu_bluebox', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_content_Nabu.svg'
));

// Icon for Green Box
$iconRegistry->registerIcon('dti_nabu_greenbox', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_content_Nabu.svg'
));

// Icon for Yellow Box
$iconRegistry->registerIcon('dti_nabu_yellowbox', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_content_Nabu.svg'
));

// Icon for HeadImage
$iconRegistry->registerIcon('dti_nabu_slider', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_Slideshow_NABU.svg'
));

// Icon for Project
$iconRegistry->registerIcon('dti_nabu_project', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_content_Nabu.svg'
));

// Icon for Contact person
$iconRegistry->registerIcon('dti_nabu_contact', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_content_Nabu.svg'
));

// Icon for Disturb
$iconRegistry->registerIcon('dti_nabu_disturb', \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class, array(
    'source' => 'EXT:dti_nabu/Resources/Public/Icons/Content/Backend_content_Nabu.svg'
));
