<?php
namespace DTInternet\DtiNabu\Controller;
/*
 *
 * Copyright (c) 2016 Stefan Bublies project@sbublies.de
 *
 * Author Stefan Bublies, project@sbublies.de
 * @link     http://www.sbublies.de
 * @package  NABU TYPO3 CMS Vorlage
 *
 */


/**
 * Controller: Slideshow
 *
 * @package DTInternet\DtiNabu\Controller
 */

use TYPO3\CMS\Core\Utility\DebugUtility;

class DtiNabuSlideshowController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \DTInternet\DtiNabu\Domain\Repository\SlideRepository
     *
     */
    protected $slideRepository;

		/**
		 * @param \DTInternet\DtiNabu\Domain\Repository\SlideRepository|null $slideRepository
		 */
		public function __construct(?\DTInternet\DtiNabu\Domain\Repository\SlideRepository $slideRepository)
		{
			$this->slideRepository = $slideRepository;
		}


	/**
     * Zeige die Slides in der Slideshow
     *
     * @return void
     */
    public function showAction(): \Psr\Http\Message\ResponseInterface
    {
        $currentId = $this->configurationManager->getContentObject()->data;
        $this->view->assignMultiple(array(
            'identifier' => 'dti-nabu-carousel-id-' . $currentId['uid'],
            'slides' => $this->slideRepository->findByContentId($currentId['uid']),
        ));
        return $this->htmlResponse();
    }
}
