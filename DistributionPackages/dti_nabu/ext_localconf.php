<?php

use DTInternet\DtiNabu\Controller\DtiNabuSlideshowController;

defined('TYPO3') or die();

$year = date("Y");

// Backend Login Styles
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginLogo'] = 'EXT:dti_nabu/Resources/Public/Images/BackendLogin/nabu_logo.jpg';
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginHighlightColor'] = '#0068B4';
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginFootnote'] = 'Copyright TYPO3 NABU-Template '. $year . ' by Stefan Bublies';

// Konfiguration des RTE für das NABU-Template

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['NabuRTE'] = 'EXT:dti_nabu/Configuration/RTE/NabuRTE.yaml';

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DtiNabu',
    'DtiNabuSlideshow',
    array(
	    DtiNabuSlideshowController::class => 'show',
    ),
    // non-cacheable actions
    array(
	    DtiNabuSlideshowController::class => 'show',
    )
);
