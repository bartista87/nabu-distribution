<?php
if (!defined('TYPO3')) {
    die ('Access denied.');
}

return array(
    'ctrl' => array(
        'title' => 'Slide',
        'label' => 'title',
        'hideAtCopy' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'editlock' => 'editlock',
        'dividers2tabs' => true,
        'hideTable' => true,
        'sortby' => 'sorting',
        'delete' => 'deleted',
        'searchFields' => 'uid, title',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'iconfile' => 'EXT:dti_nabu/Resources/Public/Icons/tx_dti_nabu_domain_model_slide.gif',
    ),
    'interface' => array(
        'showRecordFieldList' => 'hidden, title, link, link_text, visual'
    ),
    'types' => array(
        0 => array(
            'showitem' => 'title;;access, link, link_text, visual'
        )
    ),
    'palettes' => array(
        'access' => array(
            'showitem' => 'hidden, starttime, endtime',
            'canNotCollapse' => false
        )
    ),
    'columns' => array(
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config' => array(
                'type' => 'check',
                'default' => 0
            )
        ),
        'starttime' => Array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.php:LGL.starttime',
            'config' => Array(
                'type' => 'input',
                'size' => '10',
                'max' => '20',
                'eval' => 'datetime',
                'checkbox' => '0',
                'default' => '0'
            )
        ),
        'endtime' => Array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.php:LGL.endtime',
            'config' => Array(
                'type' => 'input',
                'size' => '8',
                'max' => '20',
                'eval' => 'datetime',
                'checkbox' => '0',
                'default' => '0',
                'range' => Array(
                    'upper' => mktime(0, 0, 0, 12, 31, 2020),
                    'lower' => mktime(0, 0, 0, date('m') - 1, date('d'), date('Y'))
                )
            )
        ),
        'title' => array(
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'Überschrift',
            'config' => array(
                'type' => 'text',
                'eval' => 'trim, required',
                'size' => 30,
            )
        ),
        'link' => array(
            'exclude' => 0,
            'label' => 'Link',
            'config' => array(
                'type' => 'input',
                'size' => '30',
                'softref' => 'typolink',
                'wizards' => array(
                    '_PADDING' => 2,
                    'link' => array(
                        'type' => 'popup',
                        'title' => 'Link',
                        'icon' => 'actions-wizard-link',
                        'module' => array(
                            'name' => 'wizard_element_browser',
                            'urlParameters' => array(
                                'mode' => 'wizard'
                            ) ,
                        ) ,
                        'JSopenParams' => 'height=600,width=500,status=0,menubar=0,scrollbars=1'
                    )
                )
            )
        ),
        'link_text' => array(
            'exclude' => 0,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'Link Text',
            'config' => array(
                'type' => 'input',
                'size' => 30,
            )
        ),
        'visual' => array(
            'label' => 'Bild',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'visual',
                array(
                    'appearance' => array(
                        'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:media.addFileReference'
                    ),
                    'minitems' => 1,
                    'maxitems' => 1,
                ),
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ),
    ),
);
