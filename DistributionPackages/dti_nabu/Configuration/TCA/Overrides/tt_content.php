<?php
/*
 *
 * Copyright (c) 2016 D&T Internet GmbH bublies@dt-internet.de
 * Anpassungen und umfangreiche Änderungen 2021 by Stefan Bublies, project@sbublies.de
 * Author Stefan Bublies, bublies@dt-internet.de
 * @link     http://www.dt-internet.de
 * @package  NABU TYPO3 CMS Vorlage
 *
 */

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(function () {

  $languageFilePrefix = 'LLL:EXT:fluid_styled_content/Resources/Private/Language/Database.xlf:';
  $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

  // Add ContentElement Blue Box
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
      array(
          'Blaue Box',
          'dti_nabu_bluebox',
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
          .
          'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
      ),
      'CType',
      'dti_nabu'
  );

	// Add ContentElement Blue Box
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
		array(
			'Blaue Box Hauptinhalt',
			'dti_nabu_bluebox_content',
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
			.
			'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
		),
		'CType',
		'dti_nabu'
	);

  // Add ContentElement Green Box
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
      array(
          'Grüne Box',
          'dti_nabu_greenbox',
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
          .
          'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
      ),
      'CType',
      'dti_nabu'
  );

  // Add ContentElement Yellow Box
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
      array(
          'Gelbe Box',
          'dti_nabu_yellowbox',
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
          .
          'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
      ),
      'CType',
      'dti_nabu'
  );

	// Add ContentElement Yellow Box
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
		array(
			'Gelbe Box Hauptinhalt',
			'dti_nabu_yellowbox_content',
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
			.
			'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
		),
		'CType',
		'dti_nabu'
	);

  // Add ContentElement Kopfbild
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
      array(
          'Kopfbild',
          'dti_nabu_slider',
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
          .
          'Resources/Public/Icons/Content/Backend_Slideshow_NABU.svg'
      ),
      'CType',
      'dti_nabu'
  );

  // Add ContentElement Project
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
      array(
          'Projekt-Element',
          'dti_nabu_project',
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
          .
          'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
      ),
      'CType',
      'dti_nabu'
  );

  // Add ContentElement Contact Person
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
      array(
          'Ansprechpartner',
          'dti_nabu_contact',
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
          .
          'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
      ),
      'CType',
      'dti_nabu'
  );

    // Add ContentElement Disturb
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            'Fusszeilen-Störer',
            'dti_nabu_disturb',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
            .
            'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
        ),
        'CType',
        'dti_nabu'
    );

	// Add ContentElement Topic List
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
		array(
			'Themenliste',
			'dti_nabu_topiclist',
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('dti_nabu')
			.
			'Resources/Public/Icons/Content/Backend_content_Nabu.svg'
		),
		'CType',
		'dti_nabu'
	);

	$GLOBALS['TCA']['tt_content']['types']['dti_nabu_topiclist'] = array(
		'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_yellowbox_link;' . 'Link,
    dti_nabu_linktext;' . 'Linkbeschriftung,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
	'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true' ]]]
	);


// Configure the Default Backend Fields for ContentElement "Blue Box"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_bluebox'] = array(
    'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_yellowbox_link;' . 'Link,
    dti_nabu_linktext;' . 'Linkbeschriftung,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
		assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
);

// Configure the Default Backend Fields for ContentElement "Blue Box"
	$GLOBALS['TCA']['tt_content']['types']['dti_nabu_bluebox_content'] = array(
		'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_class;' . 'CSS Klasse (für 100%: large second-large | für 50%: col-sm-6),
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
		'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
	);

// Configure the Default Backend Fields for ContentElement "Green Box"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_greenbox'] = array(
    'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_yellowbox_link;' . 'Link,
    dti_nabu_linktext;' . 'Linkbeschriftung,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
		assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
	'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
);

// Configure the Default Backend Fields for ContentElement "Yellow Box"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_yellowbox'] = array(
    'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_yellowbox_link;' . 'Link,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
	'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
);

// Configure the Default Backend Fields for ContentElement "Yellow Box"
	$GLOBALS['TCA']['tt_content']['types']['dti_nabu_yellowbox_content'] = array(
		'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_class;' . 'CSS Klasse (für 100%: large second-large| für 50%: col-sm-6),
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
		'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
	);

// Configure the Default Backend Fields for ContentElement "Kopfbild"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_slider'] = array(
    'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    dti_nabu_yellowbox_link;' . 'Link,
    dti_nabu_linktext;' . 'Linkbeschriftung,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
);

// Configure the Default Backend Fields for ContentElement "Project"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_project'] = array(
    'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_yellowbox_link;' . 'Link zum Projekt,
    dti_nabu_linktext;' . 'Linkbeschriftung,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
	'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
);

// Configure the Default Backend Fields for ContentElement "Contact Person"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_contact'] = array(
    'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . 'Name Ansprechpartner,
    dti_nabu_contactfunc;' . 'Funktion,
    dti_nabu_contactstreet;' . 'Strasse / Hausnummer,
    dti_nabu_contactplzort;' . 'PLZ Ort,
    dti_nabu_contactmail;' . 'E-Mail Adresse,
    dti_nabu_contactphone;' . 'Telefonnummer,
    dti_nabu_contactfax;' . 'Fax,
    dti_nabu_contactmobile;' . 'Handynummer,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
);

// Configure the Default Backend Fields for ContentElement "Disturb"
$GLOBALS['TCA']['tt_content']['types']['dti_nabu_disturb'] = array(
        'showitem' => '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    header;' . $frontendLanguageFilePrefix . 'palette.header,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    dti_nabu_disturbcolor;' . 'Hintergrundfarbe des Störers,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
		assets,
     --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden
',
	'columnsOverrides' => ['bodytext' => ['config' => ['enableRichtext' => 'true']]]
 );

// New TCA Fields in TT-Content
$tempColumns = array(
    'dti_nabu_yellowbox_link' => [
        'label' => 'Link',
        'exclude' => 1,
        'config' => [
          'type' => 'input',
	        'renderType' => 'inputLink',
      ],
    ],
    'dti_nabu_linktext' => array(
        'exclude' => 0,
        'label' => 'Linktext',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'dti_nabu_contactmail' => array(
        'exclude' => 0,
        'label' => 'Mailadresse',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'dti_nabu_contactphone' => array(
        'exclude' => 0,
        'label' => 'Telefon',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'dti_nabu_contactfax' => array(
        'exclude' => 0,
        'label' => 'Fax',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'dti_nabu_contactmobile' => array(
        'exclude' => 0,
        'label' => 'Handy',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
		'dti_nabu_contactfunc' => array(
			'exclude' => 0,
			'label' => 'Funktion',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dti_nabu_contactstreet' => array(
			'exclude' => 0,
			'label' => 'Straße',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dti_nabu_contactplzort' => array(
			'exclude' => 0,
			'label' => 'PLZ/Ort',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dti_nabu_class' => array(
			'exclude' => 0,
			'label' => 'CSS Klasse',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
    'dti_nabu_slides' => array(
        'exclude' => 1,
        'label' => 'NABU Slides',
        'config' => array(
            'type' => 'inline',
            'foreign_table' => 'tx_dtinabu_domain_model_slide',
            'foreign_field' => 'content_id',
            'foreign_sortby' => 'sorting',
            'maxitems' => '9999',
            'appearance' => array(
                'collapseAll' => true,
                'expandSingle' => true,
                'useSortable' => true,
                'enabledControls' => true,
                'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:media.addFileReference',
                'newRecordLinkAddTitle' => true,
                'levelLinksPosition' => 'top',
                'showSynchronizationLink' => false,
                'showAllLocalizationLink' => true,
                'showPossibleLocalizationRecords' => true,
                'showRemovedLocalizationRecords' => true,
            )
        ),
    ),
    'dti_nabu_disturbcolor' => array(
        'exclude' => 0,
        'label' => 'Hintergrundfarbe des Störers',
        'config' => array(
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => array(
                array('Standard','orange'),
                array('Gelb', 'yellow'),
                array('Grün', 'green'),
                array('Blau', 'blue')
            ),
            'size' => 1,
            'maxItems' => 1
        )
    )
);

// Add the Fields to TCA
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $tempColumns
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['dtinabu_dtinabuslideshow'] = 'layout, select_key, pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dtinabu_dtinabuslideshow'] = 'pi_flexform, dti_nabu_slides';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'dtinabu_dtinabuslideshow',
    'FILE:EXT:dti_nabu/Configuration/FlexForm/Slideshow.xml'
);

	\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
		(
		new \B13\Container\Tca\ContainerConfiguration(
			'dti-2cols', // CType
			'2 Spalten Element', // label
			'2 Spalten Element für den Hauptinhalt', // description
			[
				[
					['name' => 'Linke Spalte', 'colPos' => 201],
					['name' => 'Rechte Spalte', 'colPos' => 202]
				]
			] // grid configuration
		)
		)
			// set an optional icon configuration
			->setIcon('EXT:container/Resources/Public/Icons/container-2col.svg')
	);

});
