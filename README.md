# NABU Template auf Basis TYPO3 CMS 12 LTS

## Systemvoraussetzungen
* Min. PHP 8.1
* Composer
* Git client

## Installation
1. Projekt clonen

        git clone https://gitlab.com/bartista87/nabu-distribution.git
2. In das geklonte Projekt-Verszeichnis wechseln

        cd nabu-distribution/
3. Mit Projekt-Abhängigkeiten per Composer installieren

        composer install
4. Typo3 per Web-Interface konfigurieren (s. a. [Typo3 Install](https://docs.typo3.org/m/typo3/tutorial-getting-started/11.5/en-us/Installation/Install.html#install))
5. Extension einrichten (wenn mehrere PHP-Versionen installiert, muss ggf. richtiges PHP-Binary voran gestellt werden, z.B. php8.1)

        vendor/bin/typo3 extension:setup

## Konfiguration in Typo3
1. Site einrichten
2. Template konfigurieren (Nabu Template inkludieren)
3. Inhalte einpflegen, Nabu-Elemente verwenden

## Beteiligung
Helfer sind immer Willkommen! Verbesserungen und Ergänzungen können als Merge Request eingereicht werden.
